﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace StoredProcedureDependencies
{
    internal class Program
    {
        private static readonly Dictionary<string, string> _availableEnvironments = new Dictionary<string, string>
        {
            { "development", "Application Name=StoredProcedureDependencies;data source=sql.inncenter.pms.inrd.io;User ID=sysinnRoadUAT;Password=pre$a@E9;Initial Catalog=inncenter01p" },
            { "qa", "Application Name=StoredProcedureDependencies;data source=sql-inncenter.qainnroad.com;User ID=sysinnRoadUAT;Password=pre$a@E9;Initial Catalog=inncenter01p" },
            { "qa2", "Application Name=StoredProcedureDependencies;data source=sql-inncenter.qa2innroad.com;User ID=sysinnRoadUAT;Password=pre$a@E9;Initial Catalog=inncenter01p" },
            { "qa3", "Application Name=StoredProcedureDependencies;data source=sql-inncenter.qa3innroad.com;User ID=sysinnRoadUAT;Password=pre$a@E9;Initial Catalog=inncenter01p" }
        };

        private static readonly Dictionary<string, HashSet<string>> _columnsByTable = new Dictionary<string, HashSet<string>>();
        private static readonly Queue<string> _spQueue = new Queue<string>();
        private static readonly HashSet<string> _processedSP = new HashSet<string>();
        private static string _currentConnection = string.Empty;

        private static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Please provide required arguments");
                Console.WriteLine(">> StoredProcedureDependencies <Development|QA|QA2|QA3> <Text file path containing List of SPs>");
                return;
            }

            var envName = args[0].Trim();
            if (!_availableEnvironments.TryGetValue(envName.Trim().ToLower(), out _currentConnection))
            {
                Console.WriteLine($"Error: Valid environment names are { string.Join(", ", _availableEnvironments.Keys) }");
                return;
            }

            var filePath = args[1].Trim();
            if (!File.Exists(filePath))
            {
                Console.WriteLine($"Error: Could not find file { filePath }");
                return;
            }

            var spNames = File.ReadAllLines(filePath);

            foreach (var spName in spNames)
            {
                if (string.IsNullOrEmpty(spName) || string.IsNullOrWhiteSpace(spName))
                {
                    continue;
                }

                var trimmedSpName = spName.Trim();

                if (!trimmedSpName.Contains("."))
                {
                    trimmedSpName = "dbo." + trimmedSpName;
                }

                Console.WriteLine($"Processing SP: {trimmedSpName}");
                _spQueue.Enqueue(trimmedSpName);
                while (_spQueue.Any())
                {
                    var processingSP = _spQueue.Dequeue();

                    Console.WriteLine($"        Processing SP: {processingSP}");
                    ProcessSP(processingSP);
                }
                Console.WriteLine();
            }

            Console.WriteLine("-----------------Tables-----------------");

            foreach (var e in _columnsByTable)
            {
                Console.WriteLine($"{e.Key}");
            }

            Console.WriteLine("\n-----------------Columns-----------------\n");

            foreach (var e in _columnsByTable)
            {
                Console.WriteLine($"{e.Key}");
                Console.WriteLine(new string('-', e.Key.Length));
                foreach (var column in e.Value)
                {
                    Console.WriteLine(column);
                }
                Console.WriteLine("\n");
            }
        }

        private static void ProcessSP(string spName)
        {
            var query = $@"SELECT s.name [Schema],
                                  e.referenced_entity_name [TableName],
                                  e.referenced_minor_name [ColumnName],
                                  o.type [Type]
                          FROM sys.dm_sql_referenced_entities ('{spName}', 'OBJECT') e
                          LEFT JOIN sys.all_objects o ON e.referenced_id = o.object_id
                          LEFT JOIN sys.schemas s ON s.schema_id = o.schema_id";
            using (var connection = new SqlConnection(_currentConnection))
            {
                var result = connection.Query<SQLObject>(query, commandType: CommandType.Text);
                foreach (var obj in result)
                {
                    var type = (obj.Type ?? string.Empty).Trim();
                    if (type.Equals("P") || type.Equals("TF") || type.Equals("V") || string.IsNullOrEmpty(type))
                    {
                        var fullyQualifiedName = string.Join(".", new[] { obj.Schema ?? "dbo", obj.TableName });
                        if (_processedSP.Contains(fullyQualifiedName)) continue;
                        _spQueue.Enqueue(fullyQualifiedName);
                        _processedSP.Add(fullyQualifiedName);
                        continue;
                    }

                    if (!string.IsNullOrEmpty(obj.ColumnName) && type.Equals("U"))
                    {
                        var fullName = string.Join(".", obj.Schema, obj.TableName).ToLower();
                        if (_columnsByTable.TryGetValue(fullName, out var columns))
                        {
                            columns.Add(obj.ColumnName);
                        }
                        else
                        {
                            _columnsByTable.Add(fullName, new HashSet<string> { obj.ColumnName });
                        }
                    }
                }
            }
        }
    }

    internal class SQLObject
    {
        public string Schema { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string Type { get; set; }
    }
}