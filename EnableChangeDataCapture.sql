exec msdb.dbo.rds_cdc_enable_db 'inncenter01p'
exec sp_addrolemember 'db_owner', 'cdc'

--https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.SQLServer.html

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'user_ref_property_xref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'user_ref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'territory',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'sys_ledgerAccountType_lov',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'source',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'roomInventoryExclude',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'roomClass_ref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'roomClass_published',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'room',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'reservation_status',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'reservation_log',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'reservation_address_ref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'reservation',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'property_ref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'property_published',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'property_lock_xref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'property_address_published',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'folio_ledger_trans',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'client_ledgerAccount_lov',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'client',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'calendar_date',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'account_reservation_folio_xref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'account_ref',
@role_name = NULL,
@supports_net_changes = 1

exec sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'EntitySection_ref',
@role_name = NULL,
@supports_net_changes = 1

GO

EXEC sys.sp_cdc_change_job @job_type = 'capture' ,@pollinginterval = 86400