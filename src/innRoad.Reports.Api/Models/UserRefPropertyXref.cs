﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class UserRefPropertyXref
    {
        public int UserRefId { get; set; }
        public int PropertyId { get; set; }
        public int IsDefault { get; set; }
    }
}
