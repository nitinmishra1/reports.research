﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class PropertyPublished
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyDescriptionShort { get; set; }
        public string PropertyDescriptionLong { get; set; }
        public string PropertyLogo { get; set; }
        public string PropertyPicture { get; set; }
        public string PropertyUrl { get; set; }
        public bool UseMailingInfo { get; set; }
        public DateTime LastUpdated { get; set; }
        public string LegalName { get; set; }
        public string Policy { get; set; }
    }
}
