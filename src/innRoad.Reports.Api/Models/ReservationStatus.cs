﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class ReservationStatus
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
