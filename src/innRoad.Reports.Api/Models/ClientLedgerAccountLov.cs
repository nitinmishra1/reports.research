﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class ClientLedgerAccountLov
    {
        public int LedgerAccountId { get; set; }
        public int? ClientId { get; set; }
        public string LedgerAccountNumber { get; set; }
        public string LedgerAccountName { get; set; }
        public string LedgerAccountDescription { get; set; }
        public int LedgerAccountTypeId { get; set; }
        public int? ParentLedgerAccountId { get; set; }
        public bool IsSystemAccount { get; set; }
        public int? Active { get; set; }
        public bool? IsCreditCard { get; set; }
        public decimal? DefaultAmount { get; set; }
        public int? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
    }
}
