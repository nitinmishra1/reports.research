﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class Source
    {
        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceUrl { get; set; }
        public int? Active { get; set; }
        public int? ClientId { get; set; }
        public string Policy { get; set; }
        public int? ParentId { get; set; }
        public bool? IsShow { get; set; }
    }
}
