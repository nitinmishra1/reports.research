﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class RoomClassPublished
    {
        public int RoomClassId { get; set; }
        public string RoomClassName { get; set; }
        public string RoomClassDescription { get; set; }
        public string RoomClassPicture { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Policy { get; set; }
        public string RoomclassAbrv { get; set; }
    }
}
