﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class RoomClassRef
    {
        public int RoomClassId { get; set; }
        public int PropertyId { get; set; }
        public int MaxAdults { get; set; }
        public int MaxPersons { get; set; }
        public bool Published { get; set; }
        public int Active { get; set; }
        public int? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? RmclSortOrder { get; set; }
        public bool? SmokingAllowed { get; set; }
        public double? RoomSizeValue { get; set; }
        public string RoomSizeUnit { get; set; }
        public int? RoomClassViewId { get; set; }
        public bool? PartiesAllowed { get; set; }
        public bool? PetsAllowed { get; set; }
    }
}
