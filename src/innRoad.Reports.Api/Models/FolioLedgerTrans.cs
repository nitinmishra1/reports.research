﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class FolioLedgerTrans
    {
        public int TransactionId { get; set; }
        public int FolioId { get; set; }
        public int? TransactionStatusId { get; set; }
        public int? LedgerAccountId { get; set; }
        public string DisplayCaption { get; set; }
        public int? RoomId { get; set; }
        public int? ReservationId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? DateEffective { get; set; }
        public string Note { get; set; }
        public string ItemDetail { get; set; }
        public DateTime? DateStatement { get; set; }
        public int? ReferenceId { get; set; }
        public int? ReferenceTypeId { get; set; }
        public bool Master { get; set; }
        public DateTime? DateInsert { get; set; }
        public int? UserInsert { get; set; }
        public DateTime? DateStatus { get; set; }
        public int? UserStatus { get; set; }
        public int? PropertyId { get; set; }
        public bool? IsPercent { get; set; }
        public decimal? TaxItemValue { get; set; }
        public bool? IsTaxable { get; set; }
        public bool? IsIgnore { get; set; }
        public bool? IsAutoCreated { get; set; }
        public int? UserAudited { get; set; }
        public DateTime? DateAudited { get; set; }
        public DateTime? DateCommission { get; set; }
        public bool? IsNetExpense { get; set; }
        public int? PackageQuantity { get; set; }
        public int? ParentLedgerAccountId { get; set; }
        public bool? DoNotRoute { get; set; }
        public string ParentItemId { get; set; }
        public string TemplateItemId { get; set; }
        public string ItemId { get; set; }
        public bool? IsVat { get; set; }
        public string ParentPaymentItemId { get; set; }
        public int? DerivedRateId { get; set; }
        public DateTime? DueDate { get; set; }
    }
}
