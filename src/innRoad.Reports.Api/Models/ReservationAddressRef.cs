﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class ReservationAddressRef
    {
        public int ReservationId { get; set; }
        public int AddressTypeId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public int? TerritoryId { get; set; }
        public string PostalCode { get; set; }
        public int? CountryId { get; set; }
        public string PhoneAreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExtension { get; set; }
        public string EMail { get; set; }
        public string AltPhoneAreaCode { get; set; }
        public string AltPhoneNumber { get; set; }
        public string AltPhoneExtension { get; set; }
        public string FaxAreaCode { get; set; }
        public string FaxNumber { get; set; }
        public string FaxExtension { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string PhoneCountryCode { get; set; }
        public string AltphoneCountryCode { get; set; }
        public string FaxCountryCode { get; set; }
        public string Salutation { get; set; }
        public int? PropertyId { get; set; }
        public int? ClientId { get; set; }
    }
}
