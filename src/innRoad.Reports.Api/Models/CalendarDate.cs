﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class CalendarDate
    {
        public DateTime CalendarDateId { get; set; }
        public int CalendarYear { get; set; }
        public int CalendarMonth { get; set; }
        public int CalendarDay { get; set; }
        public int CalendarDayOfWeek { get; set; }
        public int BinWeekDay { get; set; }
    }
}
