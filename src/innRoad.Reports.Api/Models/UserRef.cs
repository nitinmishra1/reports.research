﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class UserRef
    {
        public int UserId { get; set; }
        public string LoginId { get; set; }
        public int ClientId { get; set; }
        public string UserNameLast { get; set; }
        public string UserNameFirst { get; set; }
        public string Email { get; set; }
        public string UserPassword { get; set; }
        public int Active { get; set; }
        public bool IsPropertyAlwaysAvailable { get; set; }
        public int UserUpdate { get; set; }
        public DateTime DateUpdate { get; set; }
        public DateTime? CurrentLoginTime { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public DateTime? LastLogoutTime { get; set; }
        public DateTime? PasswordExpiryDate { get; set; }
        public int? FailedLoginAttempt { get; set; }
        public DateTime? FailedLoginTime { get; set; }
    }
}
