﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class AccountRef
    {
        public int AccountId { get; set; }
        public int ClientId { get; set; }
        public string AccountName { get; set; }
        public bool? UseMailingInfo { get; set; }
        public int Active { get; set; }
        public string AccountNo { get; set; }
        public DateTime? AccountSince { get; set; }
        public int? AccountType { get; set; }
        public string DateExpiration { get; set; }
        public string BillingAccountNumber { get; set; }
        public int? BillingTypeId { get; set; }
        public string AccountFirstName { get; set; }
        public string AccountLastName { get; set; }
        public string SocialSecurityNo { get; set; }
        public string SpecialInstructions { get; set; }
        public string AccountNoAlpha { get; set; }
        public int? AccountPaymentTypeId { get; set; }
        public bool? TaxExempt { get; set; }
        public string TaxExemptId { get; set; }
        public string Notes2 { get; set; }
        public string CcLast4Digits { get; set; }
        public int? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string Salutation { get; set; }
        public int? MarketSegmentValue { get; set; }
        public DateTime? DateArrival { get; set; }
        public DateTime? DateDeparture { get; set; }
        public int? HouseAccountId { get; set; }
        public int? GcAccountId { get; set; }
        public int? GcId { get; set; }
        public int? ReferralValue { get; set; }
        public int? AssociatedAccountId { get; set; }
        public string AssociatedAccountName { get; set; }
        public bool? SuppressPrinting { get; set; }
        public int? AgingPaymenttermsTypeId { get; set; }
        public int? PaymentTerms { get; set; }
        public int Crperiod { get; set; }
        public string ImportId { get; set; }
    }
}
