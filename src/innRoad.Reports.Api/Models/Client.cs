﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class Client
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public bool UseMailingInfo { get; set; }
        public int Active { get; set; }
        public string ClientCode { get; set; }
        public string Policy { get; set; }
        public int? CurrencyId { get; set; }
        public double? TimeZone { get; set; }
        public int? UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? DefaultDistributionMethod { get; set; }
        public string DefaultTokenProvider { get; set; }
        public int? AutoMaskPhoneReq { get; set; }
        public bool? ChkTaxDefaultInFolio { get; set; }
        public bool? ChkShowTaxInFolio { get; set; }
        public string TaxId { get; set; }
        public string LblTax { get; set; }
        public bool? IncludeGuestName { get; set; }
        public bool? IsLongReservation { get; set; }
        public int? LongReservationFormat { get; set; }
        public int? LongReservationValue { get; set; }
        public string RequireAddressFields { get; set; }
        public bool? IncludeArrivalDepartureDatesAndNights { get; set; }
        public bool? EnableAdvanceDeposit { get; set; }
        public int? UseRatesFrom { get; set; }
        public int AgingRecievablesTypeId { get; set; }
        public int? AgingPaymenttermsTypeId { get; set; }
        public int? PaymentTerms { get; set; }
        public int Crperiod { get; set; }
        public int IsIncrementalRelization { get; set; }
        public bool? IsEnableDailyTrialBalance { get; set; }
        public bool? EnableAdvancedGroups { get; set; }
        public bool? EnableEmergencyReports { get; set; }
        public string EmergencyReportsToEmailIds { get; set; }
        public bool? IsEnableScheduling { get; set; }
        public bool IsEnableActivePolicy { get; set; }
        public bool EnableSuiteLogic { get; set; }
        public string CrmpropertyName { get; set; }
        public bool Isbillable { get; set; }
        public bool EnableNextGen { get; set; }
        public bool? EnableComplexRatesGrid { get; set; }
        public short StartDayOfWeek { get; set; }
        public int DateFormatId { get; set; }
        public bool? EnableVatoption { get; set; }
        public string InvoiceNumberPrefix { get; set; }
        public string BusinessVatregNumber { get; set; }
    }
}
