﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class EntitySectionRef
    {
        public int SectionId { get; set; }
        public int? ReferenceTypeId { get; set; }
        public int? ReferenceId { get; set; }
        public string SectionName { get; set; }
        public string SectionDetails { get; set; }
        public bool? IsSystem { get; set; }
        public int? Active { get; set; }
        public bool? ShowInBookingEngine { get; set; }
        public int? ShowInBookingEngineAs { get; set; }
        public int? UserInsert { get; set; }
        public DateTime? InsertedDate { get; set; }
        public int? UserUpdate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? SectionWidth { get; set; }
        public int? SectionHeight { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsPolicy { get; set; }
    }
}
