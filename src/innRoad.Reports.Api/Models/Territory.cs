﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class Territory
    {
        public int TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public string TerritoryAbrv { get; set; }
        public int? CountryId { get; set; }
        public int? RegionId { get; set; }
    }
}
