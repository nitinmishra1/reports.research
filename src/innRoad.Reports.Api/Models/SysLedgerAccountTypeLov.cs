﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class SysLedgerAccountTypeLov
    {
        public int LedgerAccountTypeId { get; set; }
        public string LedgerAccountTypeName { get; set; }
        public string LedgerAccountTypeDescription { get; set; }
    }
}
