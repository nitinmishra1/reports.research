﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class PropertyRef
    {
        public int PropertyId { get; set; }
        public int ClientId { get; set; }
        public int TownId { get; set; }
        public bool OnlineReservations { get; set; }
        public bool RequireGuestNames { get; set; }
        public bool Published { get; set; }
        public int Active { get; set; }
        public bool? CreateFolio { get; set; }
        public int? Authorize { get; set; }
        public decimal? PercentAuthorize { get; set; }
        public bool? IncludeCconExtRes { get; set; }
        public int? AuthorizeOnExtRes { get; set; }
        public decimal? PercentAuthorizeOnExtRes { get; set; }
        public int? QuoteValidDays { get; set; }
        public bool? AssignRoomsAutomatic { get; set; }
        public bool? ConfirmRoomatCheckIn { get; set; }
        public bool? SelectTransferDesc { get; set; }
        public bool? AllowBlankDateOnManualPayment { get; set; }
        public bool? SetRoomstatusDirtyOnCheckOut { get; set; }
        public bool? PostCancellationFee { get; set; }
        public bool? VoidCancellation { get; set; }
        public int? VoidCancellationChargeTypeId { get; set; }
        public int? PostCancellationFeeType { get; set; }
        public int? PostCancellationChargeType { get; set; }
        public double? PostCancellationFeeValue { get; set; }
        public int? PostCancellationDurationType { get; set; }
        public double? PostCancellationdurationValue { get; set; }
        public bool? PostNoShowFee { get; set; }
        public bool? VoidNoShow { get; set; }
        public int? VoidNoShowChargeTypeId { get; set; }
        public int? PostNoshowFeeType { get; set; }
        public int? PostNoShowChargeType { get; set; }
        public double? PostNoShowFeeValue { get; set; }
        public bool? RequireReferrals { get; set; }
        public bool? RequireMarketSegment { get; set; }
        public int? UserUpdate { get; set; }
        public DateTime? Dateupdate { get; set; }
        public string DefaultTimeForHseKeepingTask { get; set; }
        public int? IncludeRoomOnGuestFolios { get; set; }
        public int? IncludeRoomOnFolioAlways { get; set; }
        public int? AddRmToDepartures { get; set; }
        public int? ComputeTacomOnCheckOut { get; set; }
        public int? RequireCreditCard { get; set; }
        public int? EmailFrmPropSel { get; set; }
        public string EmailFrmPropSelVal { get; set; }
        public int? AccessLevel { get; set; }
        public int? HouseAccountId { get; set; }
        public string IpAddress { get; set; }
        public bool TrackGcsalesOrRedemption { get; set; }
        public decimal? PbxLimit { get; set; }
        public short? ConfirmRoomatCheckInOption { get; set; }
        public int? CallAccountingPostsTo { get; set; }
        public int? PointOfSalePostsTo { get; set; }
        public int? OtherDevicesPostsTo { get; set; }
        public bool IsBookByRoomClass { get; set; }
        public bool AllowNonZeroBalance { get; set; }
        public int? UnAssignedPostingTo { get; set; }
        public int? TemplateId { get; set; }
        public string TaxId { get; set; }
        public int? StatementUoby { get; set; }
        public DateTime? LockStartDate { get; set; }
        public int? MovieRestrictions { get; set; }
        public int? VideoOnDemandPostsTo { get; set; }
        public decimal? FixedAmountOnExtRes { get; set; }
        public int? NumberOfNightChargesOnExtRes { get; set; }
        public int? PercentAuthorizeOnExtResChargeType { get; set; }
        public bool? RequireGuestProfile { get; set; }
        public int? NoOfKeys { get; set; }
        public bool? CancelReason { get; set; }
        public string KeyEncoder { get; set; }
        public string ToStationNumber { get; set; }
        public string TerminalNumber { get; set; }
        public string CheckOutTime { get; set; }
        public bool? RequireEmailAddress { get; set; }
        public bool? IncludeRoomMove { get; set; }
        public bool? RequiredDepositForGuaranteedReservation { get; set; }
        public bool? SetReservationToGuaranteed { get; set; }
        public int? UseRoomNumbers { get; set; }
        public double? Timezone { get; set; }
        public string Setroomsdirtyat { get; set; }
        public int? LongStay { get; set; }
        public bool ShowTax { get; set; }
        public bool ShowUnassigned { get; set; }
        public short Rmsmode { get; set; }
        public bool EnableOccupancy { get; set; }
        public short? Occupancylevel { get; set; }
        public int? DefaultCreditLimitOnReservation { get; set; }
        public decimal? PercentCreditLimitOnReservation { get; set; }
        public decimal? FixedCreditLimitOnReservation { get; set; }
        public int? DefaultCreditLimitType { get; set; }
        public int? NoOfNightChargesasCreditOnReservation { get; set; }
        public int? ReleaseDays { get; set; }
        public bool? AutoRelease { get; set; }
        public string SendCcmail { get; set; }
        public string SendScheduleEmailsFrom { get; set; }
        public bool? EmailDisplayNameSelected { get; set; }
        public string EmailDisplayName { get; set; }
        public bool? SendEmailUsingInnroadEmail { get; set; }
        public bool SendEmailUsingPropertyEmail { get; set; }
        public string CheckInTime { get; set; }
        public string SmokingPolicy { get; set; }
        public bool? GenerateGuestRegOnCheckIn { get; set; }
        public bool? GenerateGuestStmtOnCheckOut { get; set; }
        public bool LongStayAllNights { get; set; }
        public bool LongStayAfterLimit { get; set; }
    }
}
