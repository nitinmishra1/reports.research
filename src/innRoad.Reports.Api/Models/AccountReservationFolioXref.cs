﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class AccountReservationFolioXref
    {
        public int FolioId { get; set; }
        public int? AccountId { get; set; }
        public int? ReservationId { get; set; }
        public int FoliostatusId { get; set; }
        public string FolioName { get; set; }
        public string FolioDescription { get; set; }
        public int? RoomId { get; set; }
        public int? IsDefault { get; set; }
        public int? ReferenceId { get; set; }
        public int? FolioType { get; set; }
        public int? ChargeRoutingOption { get; set; }
        public decimal? CreditLimit { get; set; }
        public int? AccessLevel { get; set; }
        public int? BlockId { get; set; }
        public bool? EnforceDeposite { get; set; }
        public bool? CreditCardRequired { get; set; }
        public bool? PosPosting { get; set; }
        public int? MovieRestrictions { get; set; }
        public bool? SuppressPrinting { get; set; }
    }
}
