﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class Room
    {
        public int RoomId { get; set; }
        public int RoomClassId { get; set; }
        public int Active { get; set; }
        public string RoomNumber { get; set; }
        public int? RmSortOrder { get; set; }
        public string StationId { get; set; }
        public int? RoomsetId { get; set; }
        public int? HousekeepingzoneId { get; set; }
    }
}
