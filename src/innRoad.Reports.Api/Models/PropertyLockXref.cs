﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class PropertyLockXref
    {
        public int PropertyId { get; set; }
        public DateTime PeriodDate { get; set; }
        public int LockTypeId { get; set; }
    }
}
