﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class RoomInventoryExclude
    {
        public int RoomId { get; set; }
        public DateTime DateEffective { get; set; }
        public int? ReservationId { get; set; }
        public int? Status { get; set; }
        public string UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public int? RoomAssignmentNumber { get; set; }
        public int? BlockId { get; set; }
        public int? PropertyId { get; set; }
        public int? ClientId { get; set; }
        public int RowId { get; set; }
    }
}
