﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class PropertyAddressPublished
    {
        public int PropertyId { get; set; }
        public int AddressTypeId { get; set; }
        public string ContactName { get; set; }
        public string PhoneAreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExtension { get; set; }
        public string FaxAreaCode { get; set; }
        public string FaxNumber { get; set; }
        public string FaxExtension { get; set; }
        public string Email { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public int? TerritoryId { get; set; }
        public string PostalCode { get; set; }
        public int CountryId { get; set; }
        public DateTime LastUpdated { get; set; }
        public string PhoneCountryCode { get; set; }
        public string FaxCountryCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
