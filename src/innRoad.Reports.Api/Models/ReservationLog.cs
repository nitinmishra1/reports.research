﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class ReservationLog
    {
        public int ReservationId { get; set; }
        public int SourceId { get; set; }
        public int StatusId { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public int ConfirmationNumber { get; set; }
        public string PhoneAreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExtension { get; set; }
        public string Email { get; set; }
        public string SpecialInstructions { get; set; }
        public bool UseMailingInfo { get; set; }
        public string BillingName { get; set; }
        public int? BillingTypeId { get; set; }
        public string BillingAccountNumber { get; set; }
        public string DateExpiration { get; set; }
        public string UserUpdate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public bool? DoNotMove { get; set; }
        public int? AccountId { get; set; }
        public bool? IsGroup { get; set; }
        public int? FrequentGuestId { get; set; }
        public string CompanyName { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestLastName { get; set; }
        public DateTime? BookedOn { get; set; }
        public string SecurityCode { get; set; }
        public bool? TaxExempt { get; set; }
        public string TaxExemptId { get; set; }
        public string Notes2 { get; set; }
        public string CcLast4Digits { get; set; }
        public int? PaymentReferenceId { get; set; }
        public string Salutation { get; set; }
        public int? MarketSegmentValue { get; set; }
        public int? ReferralsValue { get; set; }
        public string RatepromoCode { get; set; }
        public int? RatePlanId { get; set; }
        public int? TravelAgentId { get; set; }
        public string ExtConfirmationNumber { get; set; }
        public int? HouseAccountId { get; set; }
        public int? GcAccountId { get; set; }
        public int? GcId { get; set; }
        public int? CancellationNumber { get; set; }
        public string ExtcancellationNumber { get; set; }
        public int? SubSourceId { get; set; }
        public int? UserBooked { get; set; }
        public int? PropertyId { get; set; }
        public int? ClientId { get; set; }
        public int LogId { get; set; }
        public DateTime? PropertyTimeBookedOn { get; set; }
    }
}
