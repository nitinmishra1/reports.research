﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace innRoad.Reports.Api
{
    public partial class reportsContext : DbContext
    {
        public reportsContext()
        {
        }

        public reportsContext(DbContextOptions<reportsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountRef> AccountRef { get; set; }
        public virtual DbSet<AccountReservationFolioXref> AccountReservationFolioXref { get; set; }
        public virtual DbSet<AwsdmsApplyExceptions> AwsdmsApplyExceptions { get; set; }
        public virtual DbSet<CalendarDate> CalendarDate { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientLedgerAccountLov> ClientLedgerAccountLov { get; set; }
        public virtual DbSet<EntitySectionRef> EntitySectionRef { get; set; }
        public virtual DbSet<FolioLedgerTrans> FolioLedgerTrans { get; set; }
        public virtual DbSet<PropertyAddressPublished> PropertyAddressPublished { get; set; }
        public virtual DbSet<PropertyLockXref> PropertyLockXref { get; set; }
        public virtual DbSet<PropertyPublished> PropertyPublished { get; set; }
        public virtual DbSet<PropertyRef> PropertyRef { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<ReservationAddressRef> ReservationAddressRef { get; set; }
        public virtual DbSet<ReservationLog> ReservationLog { get; set; }
        public virtual DbSet<ReservationStatus> ReservationStatus { get; set; }
        public virtual DbSet<Room> Room { get; set; }
        public virtual DbSet<RoomClassPublished> RoomClassPublished { get; set; }
        public virtual DbSet<RoomClassRef> RoomClassRef { get; set; }
        public virtual DbSet<RoomInventoryExclude> RoomInventoryExclude { get; set; }
        public virtual DbSet<Source> Source { get; set; }
        public virtual DbSet<SysLedgerAccountTypeLov> SysLedgerAccountTypeLov { get; set; }
        public virtual DbSet<Territory> Territory { get; set; }
        public virtual DbSet<UserRef> UserRef { get; set; }
        public virtual DbSet<UserRefPropertyXref> UserRefPropertyXref { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=innsights-db.inrd.io;Database=reports;Username=sysinnroad;Password=0s2bMk6FF7Q");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountRef>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("account_ref_pkey");

                entity.ToTable("account_ref", "dbo");

                entity.Property(e => e.AccountId)
                    .HasColumnName("accountID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountFirstName)
                    .HasColumnName("accountFirstName")
                    .HasMaxLength(50);

                entity.Property(e => e.AccountLastName)
                    .HasColumnName("accountLastName")
                    .HasMaxLength(50);

                entity.Property(e => e.AccountName)
                    .HasColumnName("accountName")
                    .HasMaxLength(50);

                entity.Property(e => e.AccountNo).HasMaxLength(15);

                entity.Property(e => e.AccountNoAlpha).HasMaxLength(15);

                entity.Property(e => e.AccountPaymentTypeId).HasColumnName("accountPaymentTypeId");

                entity.Property(e => e.AccountType).HasColumnName("accountType");

                entity.Property(e => e.AgingPaymenttermsTypeId).HasColumnName("Aging_paymentterms_TypeID");

                entity.Property(e => e.AssociatedAccountId).HasColumnName("Associated_AccountID");

                entity.Property(e => e.AssociatedAccountName)
                    .HasColumnName("Associated_AccountName")
                    .HasMaxLength(100);

                entity.Property(e => e.BillingAccountNumber)
                    .HasColumnName("billingAccountNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.BillingTypeId).HasColumnName("billingTypeId");

                entity.Property(e => e.CcLast4Digits)
                    .HasColumnName("ccLast4Digits")
                    .HasMaxLength(4);

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.Crperiod).HasColumnName("CRPeriod");

                entity.Property(e => e.DateArrival).HasColumnName("dateArrival");

                entity.Property(e => e.DateDeparture).HasColumnName("dateDeparture");

                entity.Property(e => e.DateExpiration)
                    .HasColumnName("dateExpiration")
                    .HasMaxLength(50);

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.GcAccountId).HasColumnName("gcAccountID");

                entity.Property(e => e.GcId).HasColumnName("gcID");

                entity.Property(e => e.HouseAccountId).HasColumnName("houseAccountID");

                entity.Property(e => e.ImportId)
                    .HasColumnName("importId")
                    .HasMaxLength(50);

                entity.Property(e => e.MarketSegmentValue).HasColumnName("marketSegmentValue");

                entity.Property(e => e.Notes2)
                    .HasColumnName("notes2")
                    .HasMaxLength(50);

                entity.Property(e => e.ReferralValue).HasColumnName("referralValue");

                entity.Property(e => e.Salutation)
                    .HasColumnName("salutation")
                    .HasMaxLength(25);

                entity.Property(e => e.SocialSecurityNo)
                    .HasColumnName("socialSecurityNo")
                    .HasMaxLength(50);

                entity.Property(e => e.SpecialInstructions)
                    .HasColumnName("specialInstructions")
                    .HasMaxLength(255);

                entity.Property(e => e.SuppressPrinting).HasColumnName("suppressPrinting");

                entity.Property(e => e.TaxExempt).HasColumnName("taxExempt");

                entity.Property(e => e.TaxExemptId)
                    .HasColumnName("taxExemptID")
                    .HasMaxLength(50);

                entity.Property(e => e.UseMailingInfo).HasColumnName("useMailingInfo");

                entity.Property(e => e.UserUpdate).HasColumnName("userUpdate");
            });

            modelBuilder.Entity<AccountReservationFolioXref>(entity =>
            {
                entity.HasKey(e => e.FolioId)
                    .HasName("account_reservation_folio_xref_pkey");

                entity.ToTable("account_reservation_folio_xref", "dbo");

                entity.Property(e => e.FolioId)
                    .HasColumnName("folioID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccessLevel).HasColumnName("accessLevel");

                entity.Property(e => e.AccountId).HasColumnName("accountID");

                entity.Property(e => e.BlockId).HasColumnName("blockID");

                entity.Property(e => e.ChargeRoutingOption).HasColumnName("chargeRoutingOption");

                entity.Property(e => e.CreditLimit)
                    .HasColumnName("creditLimit")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.EnforceDeposite).HasColumnName("enforceDeposite");

                entity.Property(e => e.FolioDescription)
                    .HasColumnName("folioDescription")
                    .HasMaxLength(100);

                entity.Property(e => e.FolioName)
                    .HasColumnName("folioName")
                    .HasMaxLength(100);

                entity.Property(e => e.FolioType).HasColumnName("folioType");

                entity.Property(e => e.FoliostatusId).HasColumnName("foliostatusID");

                entity.Property(e => e.IsDefault).HasColumnName("isDefault");

                entity.Property(e => e.MovieRestrictions).HasColumnName("movieRestrictions");

                entity.Property(e => e.PosPosting).HasColumnName("posPosting");

                entity.Property(e => e.ReferenceId).HasColumnName("referenceID");

                entity.Property(e => e.ReservationId).HasColumnName("reservationID");

                entity.Property(e => e.RoomId).HasColumnName("roomID");

                entity.Property(e => e.SuppressPrinting).HasColumnName("suppressPrinting");
            });

            modelBuilder.Entity<AwsdmsApplyExceptions>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("awsdms_apply_exceptions");

                entity.Property(e => e.Error)
                    .IsRequired()
                    .HasColumnName("ERROR");

                entity.Property(e => e.ErrorTime).HasColumnName("ERROR_TIME");

                entity.Property(e => e.Statement)
                    .IsRequired()
                    .HasColumnName("STATEMENT");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasColumnName("TABLE_NAME")
                    .HasMaxLength(128);

                entity.Property(e => e.TableOwner)
                    .IsRequired()
                    .HasColumnName("TABLE_OWNER")
                    .HasMaxLength(128);

                entity.Property(e => e.TaskName)
                    .IsRequired()
                    .HasColumnName("TASK_NAME")
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<CalendarDate>(entity =>
            {
                entity.ToTable("calendar_date", "dbo");

                entity.Property(e => e.CalendarDateId).HasColumnName("calendarDateID");

                entity.Property(e => e.BinWeekDay).HasColumnName("binWeekDay");

                entity.Property(e => e.CalendarDay).HasColumnName("calendarDay");

                entity.Property(e => e.CalendarDayOfWeek).HasColumnName("calendarDayOfWeek");

                entity.Property(e => e.CalendarMonth).HasColumnName("calendarMonth");

                entity.Property(e => e.CalendarYear).HasColumnName("calendarYear");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("client", "dbo");

                entity.Property(e => e.ClientId)
                    .HasColumnName("clientID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AgingPaymenttermsTypeId).HasColumnName("Aging_paymentterms_TypeID");

                entity.Property(e => e.AgingRecievablesTypeId).HasColumnName("AgingRecievables_TypeID");

                entity.Property(e => e.AutoMaskPhoneReq).HasColumnName("autoMaskPhoneReq");

                entity.Property(e => e.BusinessVatregNumber)
                    .HasColumnName("BusinessVATRegNumber")
                    .HasMaxLength(20);

                entity.Property(e => e.ChkShowTaxInFolio).HasColumnName("chkShowTaxInFolio");

                entity.Property(e => e.ChkTaxDefaultInFolio).HasColumnName("chkTaxDefaultInFolio");

                entity.Property(e => e.ClientCode)
                    .HasColumnName("clientCode")
                    .HasMaxLength(25);

                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .HasColumnName("clientName")
                    .HasMaxLength(50);

                entity.Property(e => e.CrmpropertyName)
                    .HasColumnName("CRMPropertyName")
                    .HasMaxLength(100);

                entity.Property(e => e.Crperiod).HasColumnName("CRPeriod");

                entity.Property(e => e.CurrencyId).HasColumnName("CurrencyID");

                entity.Property(e => e.DateFormatId).HasColumnName("DateFormatID");

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.DefaultDistributionMethod).HasColumnName("defaultDistributionMethod");

                entity.Property(e => e.DefaultTokenProvider)
                    .HasColumnName("defaultTokenProvider")
                    .HasMaxLength(30);

                entity.Property(e => e.EnableVatoption).HasColumnName("EnableVATOption");

                entity.Property(e => e.InvoiceNumberPrefix).HasMaxLength(10);

                entity.Property(e => e.IsEnableActivePolicy).HasColumnName("isEnableActivePolicy");

                entity.Property(e => e.Isbillable).HasColumnName("ISbillable");

                entity.Property(e => e.LblTax)
                    .HasColumnName("lblTax")
                    .HasMaxLength(255);

                entity.Property(e => e.Policy).HasColumnName("policy");

                entity.Property(e => e.RequireAddressFields).HasMaxLength(30);

                entity.Property(e => e.TaxId)
                    .HasColumnName("taxID")
                    .HasMaxLength(255);

                entity.Property(e => e.TimeZone).HasColumnName("timeZone");

                entity.Property(e => e.UseMailingInfo).HasColumnName("useMailingInfo");

                entity.Property(e => e.UseRatesFrom).HasColumnName("useRatesFrom");

                entity.Property(e => e.UserUpdate).HasColumnName("userUpdate");
            });

            modelBuilder.Entity<ClientLedgerAccountLov>(entity =>
            {
                entity.HasKey(e => e.LedgerAccountId)
                    .HasName("client_ledgerAccount_lov_pkey");

                entity.ToTable("client_ledgerAccount_lov", "dbo");

                entity.Property(e => e.LedgerAccountId)
                    .HasColumnName("ledgerAccountID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.DefaultAmount)
                    .HasColumnName("defaultAmount")
                    .HasColumnType("numeric(10,2)");

                entity.Property(e => e.IsCreditCard).HasColumnName("isCreditCard");

                entity.Property(e => e.IsSystemAccount).HasColumnName("isSystemAccount");

                entity.Property(e => e.LedgerAccountDescription)
                    .HasColumnName("ledgerAccountDescription")
                    .HasMaxLength(250);

                entity.Property(e => e.LedgerAccountName)
                    .HasColumnName("ledgerAccountName")
                    .HasMaxLength(250);

                entity.Property(e => e.LedgerAccountNumber)
                    .HasColumnName("ledgerAccountNumber")
                    .HasMaxLength(250);

                entity.Property(e => e.LedgerAccountTypeId).HasColumnName("ledgerAccountTypeID");

                entity.Property(e => e.ParentLedgerAccountId).HasColumnName("parentLedgerAccountID");

                entity.Property(e => e.UserUpdate).HasColumnName("userUpdate");
            });

            modelBuilder.Entity<EntitySectionRef>(entity =>
            {
                entity.HasKey(e => e.SectionId)
                    .HasName("EntitySection_ref_pkey");

                entity.ToTable("EntitySection_ref", "dbo");

                entity.Property(e => e.SectionId)
                    .HasColumnName("SectionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ReferenceId).HasColumnName("ReferenceID");

                entity.Property(e => e.ReferenceTypeId).HasColumnName("ReferenceTypeID");

                entity.Property(e => e.SectionName).HasMaxLength(200);
            });

            modelBuilder.Entity<FolioLedgerTrans>(entity =>
            {
                entity.HasKey(e => e.TransactionId)
                    .HasName("folio_ledger_trans_pkey");

                entity.ToTable("folio_ledger_trans", "dbo");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transactionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.DateAudited).HasColumnName("dateAudited");

                entity.Property(e => e.DateCommission).HasColumnName("dateCommission");

                entity.Property(e => e.DateEffective).HasColumnName("dateEffective");

                entity.Property(e => e.DateInsert).HasColumnName("dateInsert");

                entity.Property(e => e.DateStatement).HasColumnName("dateStatement");

                entity.Property(e => e.DateStatus).HasColumnName("dateStatus");

                entity.Property(e => e.DerivedRateId).HasColumnName("derivedRateID");

                entity.Property(e => e.DisplayCaption)
                    .HasColumnName("displayCaption")
                    .HasMaxLength(255);

                entity.Property(e => e.FolioId).HasColumnName("folioID");

                entity.Property(e => e.IsAutoCreated).HasColumnName("isAutoCreated");

                entity.Property(e => e.IsIgnore).HasColumnName("isIgnore");

                entity.Property(e => e.IsNetExpense).HasColumnName("isNetExpense");

                entity.Property(e => e.IsPercent).HasColumnName("isPercent");

                entity.Property(e => e.IsTaxable).HasColumnName("isTaxable");

                entity.Property(e => e.IsVat).HasColumnName("isVAT");

                entity.Property(e => e.ItemDetail)
                    .HasColumnName("itemDetail")
                    .HasMaxLength(255);

                entity.Property(e => e.ItemId).HasMaxLength(36);

                entity.Property(e => e.LedgerAccountId).HasColumnName("ledgerAccountID");

                entity.Property(e => e.Master).HasColumnName("master");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(50);

                entity.Property(e => e.PackageQuantity).HasColumnName("packageQuantity");

                entity.Property(e => e.ParentItemId)
                    .HasColumnName("parentItemId")
                    .HasMaxLength(36);

                entity.Property(e => e.ParentLedgerAccountId).HasColumnName("parentLedgerAccountID");

                entity.Property(e => e.ParentPaymentItemId)
                    .HasColumnName("ParentPaymentItemID")
                    .HasMaxLength(36);

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.ReferenceId).HasColumnName("referenceID");

                entity.Property(e => e.ReferenceTypeId).HasColumnName("referenceTypeID");

                entity.Property(e => e.ReservationId).HasColumnName("reservationID");

                entity.Property(e => e.RoomId).HasColumnName("roomID");

                entity.Property(e => e.TaxItemValue)
                    .HasColumnName("taxItemValue")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.TemplateItemId)
                    .HasColumnName("templateItemId")
                    .HasMaxLength(36);

                entity.Property(e => e.TransactionStatusId).HasColumnName("transactionStatusID");

                entity.Property(e => e.UserAudited).HasColumnName("userAudited");

                entity.Property(e => e.UserInsert).HasColumnName("userInsert");

                entity.Property(e => e.UserStatus).HasColumnName("userStatus");
            });

            modelBuilder.Entity<PropertyAddressPublished>(entity =>
            {
                entity.HasKey(e => new { e.PropertyId, e.AddressTypeId })
                    .HasName("property_address_published_pkey");

                entity.ToTable("property_address_published", "dbo");

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.AddressTypeId).HasColumnName("addressTypeID");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("address1")
                    .HasMaxLength(50);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(50);

                entity.Property(e => e.Address3)
                    .HasColumnName("address3")
                    .HasMaxLength(50);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasMaxLength(50);

                entity.Property(e => e.ContactName)
                    .HasColumnName("contactName")
                    .HasMaxLength(50);

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.FaxAreaCode)
                    .HasColumnName("faxAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.FaxCountryCode)
                    .HasColumnName("faxCountryCode")
                    .HasMaxLength(3);

                entity.Property(e => e.FaxExtension)
                    .HasColumnName("faxExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.FaxNumber)
                    .HasColumnName("faxNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.LastUpdated).HasColumnName("last_updated");

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasMaxLength(30);

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneAreaCode)
                    .HasColumnName("phoneAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.PhoneCountryCode)
                    .HasColumnName("phoneCountryCode")
                    .HasMaxLength(3);

                entity.Property(e => e.PhoneExtension)
                    .HasColumnName("phoneExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phoneNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postalCode")
                    .HasMaxLength(9);

                entity.Property(e => e.TerritoryId).HasColumnName("territoryID");
            });

            modelBuilder.Entity<PropertyLockXref>(entity =>
            {
                entity.HasKey(e => new { e.PropertyId, e.PeriodDate, e.LockTypeId })
                    .HasName("property_lock_xref_pkey");

                entity.ToTable("property_lock_xref", "dbo");

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.PeriodDate).HasColumnName("periodDate");

                entity.Property(e => e.LockTypeId).HasColumnName("lockTypeID");
            });

            modelBuilder.Entity<PropertyPublished>(entity =>
            {
                entity.HasKey(e => e.PropertyId)
                    .HasName("property_published_pkey");

                entity.ToTable("property_published", "dbo");

                entity.Property(e => e.PropertyId)
                    .HasColumnName("propertyID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LastUpdated).HasColumnName("last_updated");

                entity.Property(e => e.LegalName)
                    .HasColumnName("legalName")
                    .HasMaxLength(255);

                entity.Property(e => e.Policy).HasColumnName("policy");

                entity.Property(e => e.PropertyDescriptionLong).HasColumnName("propertyDescriptionLong");

                entity.Property(e => e.PropertyDescriptionShort).HasColumnName("propertyDescriptionShort");

                entity.Property(e => e.PropertyLogo)
                    .HasColumnName("propertyLogo")
                    .HasMaxLength(255);

                entity.Property(e => e.PropertyName)
                    .IsRequired()
                    .HasColumnName("propertyName")
                    .HasMaxLength(50);

                entity.Property(e => e.PropertyPicture)
                    .HasColumnName("propertyPicture")
                    .HasMaxLength(255);

                entity.Property(e => e.PropertyUrl)
                    .HasColumnName("propertyURL")
                    .HasMaxLength(255);

                entity.Property(e => e.UseMailingInfo).HasColumnName("useMailingInfo");
            });

            modelBuilder.Entity<PropertyRef>(entity =>
            {
                entity.HasKey(e => e.PropertyId)
                    .HasName("property_ref_pkey");

                entity.ToTable("property_ref", "dbo");

                entity.Property(e => e.PropertyId)
                    .HasColumnName("propertyID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccessLevel).HasColumnName("accessLevel");

                entity.Property(e => e.AddRmToDepartures).HasColumnName("addRmToDepartures");

                entity.Property(e => e.AllowBlankDateOnManualPayment).HasColumnName("allowBlankDateOnManualPayment");

                entity.Property(e => e.AllowNonZeroBalance).HasColumnName("allowNonZeroBalance");

                entity.Property(e => e.AssignRoomsAutomatic).HasColumnName("assignRoomsAutomatic");

                entity.Property(e => e.AuthorizeOnExtRes).HasColumnName("authorizeOnExtRes");

                entity.Property(e => e.CallAccountingPostsTo).HasColumnName("callAccountingPostsTo");

                entity.Property(e => e.CancelReason).HasColumnName("cancelReason");

                entity.Property(e => e.CheckInTime)
                    .HasColumnName("checkInTime")
                    .HasMaxLength(30);

                entity.Property(e => e.CheckOutTime)
                    .HasColumnName("checkOutTime")
                    .HasMaxLength(15);

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.ComputeTacomOnCheckOut).HasColumnName("computeTAComOnCheckOut");

                entity.Property(e => e.ConfirmRoomatCheckIn).HasColumnName("confirmRoomatCheckIn");

                entity.Property(e => e.ConfirmRoomatCheckInOption).HasColumnName("confirmRoomatCheckInOption");

                entity.Property(e => e.Dateupdate).HasColumnName("dateupdate");

                entity.Property(e => e.DefaultCreditLimitOnReservation).HasColumnName("defaultCreditLimitOnReservation");

                entity.Property(e => e.DefaultCreditLimitType).HasColumnName("defaultCreditLimitType");

                entity.Property(e => e.DefaultTimeForHseKeepingTask)
                    .HasColumnName("defaultTimeForHseKeepingTask")
                    .HasMaxLength(15);

                entity.Property(e => e.EmailFrmPropSel).HasColumnName("emailFrmPropSel");

                entity.Property(e => e.EmailFrmPropSelVal)
                    .HasColumnName("emailFrmPropSelVal")
                    .HasMaxLength(1000);

                entity.Property(e => e.FixedAmountOnExtRes).HasColumnType("numeric(19,4)");

                entity.Property(e => e.FixedCreditLimitOnReservation).HasColumnType("numeric(19,4)");

                entity.Property(e => e.GenerateGuestRegOnCheckIn).HasColumnName("generateGuestRegOnCheckIn");

                entity.Property(e => e.GenerateGuestStmtOnCheckOut).HasColumnName("generateGuestStmtOnCheckOut");

                entity.Property(e => e.HouseAccountId).HasColumnName("houseAccountID");

                entity.Property(e => e.IncludeCconExtRes).HasColumnName("IncludeCCOnExtRes");

                entity.Property(e => e.IncludeRoomMove).HasColumnName("includeRoomMove");

                entity.Property(e => e.IncludeRoomOnFolioAlways).HasColumnName("includeRoomOnFolioAlways");

                entity.Property(e => e.IncludeRoomOnGuestFolios).HasColumnName("includeRoomOnGuestFolios");

                entity.Property(e => e.IpAddress)
                    .HasColumnName("ipAddress")
                    .HasMaxLength(50);

                entity.Property(e => e.IsBookByRoomClass).HasColumnName("isBookByRoomClass");

                entity.Property(e => e.KeyEncoder)
                    .HasColumnName("keyEncoder")
                    .HasMaxLength(100);

                entity.Property(e => e.LockStartDate).HasColumnName("lockStartDate");

                entity.Property(e => e.LongStayAfterLimit).HasColumnName("LongStay_AfterLimit");

                entity.Property(e => e.LongStayAllNights).HasColumnName("LongStay_AllNights");

                entity.Property(e => e.MovieRestrictions).HasColumnName("movieRestrictions");

                entity.Property(e => e.NoOfKeys).HasColumnName("noOfKeys");

                entity.Property(e => e.NoOfNightChargesasCreditOnReservation).HasColumnName("noOfNightChargesasCreditOnReservation");

                entity.Property(e => e.OnlineReservations).HasColumnName("onlineReservations");

                entity.Property(e => e.OtherDevicesPostsTo).HasColumnName("otherDevicesPostsTo");

                entity.Property(e => e.PbxLimit)
                    .HasColumnName("pbxLimit")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.PercentAuthorize).HasColumnType("numeric(19,4)");

                entity.Property(e => e.PercentAuthorizeOnExtRes)
                    .HasColumnName("percentAuthorizeOnExtRes")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.PercentAuthorizeOnExtResChargeType).HasColumnName("percentAuthorizeOnExtResChargeType");

                entity.Property(e => e.PercentCreditLimitOnReservation)
                    .HasColumnName("percentCreditLimitOnReservation")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.PointOfSalePostsTo).HasColumnName("pointOfSalePostsTo");

                entity.Property(e => e.PostCancellationChargeType).HasColumnName("postCancellationChargeType");

                entity.Property(e => e.PostCancellationDurationType).HasColumnName("postCancellationDurationType");

                entity.Property(e => e.PostCancellationFee).HasColumnName("postCancellationFee");

                entity.Property(e => e.PostCancellationFeeType).HasColumnName("postCancellationFeeType");

                entity.Property(e => e.PostCancellationFeeValue).HasColumnName("postCancellationFeeValue");

                entity.Property(e => e.PostCancellationdurationValue).HasColumnName("postCancellationdurationValue");

                entity.Property(e => e.PostNoShowChargeType).HasColumnName("postNoShowChargeType");

                entity.Property(e => e.PostNoShowFee).HasColumnName("postNoShowFee");

                entity.Property(e => e.PostNoShowFeeValue).HasColumnName("postNoShowFeeValue");

                entity.Property(e => e.PostNoshowFeeType).HasColumnName("postNoshowFeeType");

                entity.Property(e => e.Published).HasColumnName("published");

                entity.Property(e => e.RequireCreditCard).HasColumnName("requireCreditCard");

                entity.Property(e => e.RequireGuestNames).HasColumnName("requireGuestNames");

                entity.Property(e => e.RequireGuestProfile).HasColumnName("requireGuestProfile");

                entity.Property(e => e.RequireMarketSegment).HasColumnName("requireMarketSegment");

                entity.Property(e => e.RequireReferrals).HasColumnName("requireReferrals");

                entity.Property(e => e.Rmsmode).HasColumnName("RMSMode");

                entity.Property(e => e.SelectTransferDesc).HasColumnName("selectTransferDesc");

                entity.Property(e => e.SendCcmail)
                    .HasColumnName("sendCCMail")
                    .HasMaxLength(100);

                entity.Property(e => e.SendScheduleEmailsFrom)
                    .HasColumnName("sendScheduleEmailsFrom")
                    .HasMaxLength(100);

                entity.Property(e => e.SetRoomstatusDirtyOnCheckOut).HasColumnName("setRoomstatusDirtyOnCheckOut");

                entity.Property(e => e.Setroomsdirtyat)
                    .HasColumnName("setroomsdirtyat")
                    .HasMaxLength(30);

                entity.Property(e => e.SmokingPolicy)
                    .HasColumnName("smokingPolicy")
                    .HasMaxLength(50);

                entity.Property(e => e.StatementUoby).HasColumnName("statementUOBy");

                entity.Property(e => e.TaxId)
                    .HasColumnName("taxID")
                    .HasMaxLength(255);

                entity.Property(e => e.TemplateId).HasColumnName("templateID");

                entity.Property(e => e.TerminalNumber)
                    .HasColumnName("terminalNumber")
                    .HasMaxLength(100);

                entity.Property(e => e.Timezone).HasColumnName("timezone");

                entity.Property(e => e.ToStationNumber)
                    .HasColumnName("toStationNumber")
                    .HasMaxLength(100);

                entity.Property(e => e.TownId).HasColumnName("townID");

                entity.Property(e => e.TrackGcsalesOrRedemption).HasColumnName("trackGCSalesOrRedemption");

                entity.Property(e => e.UnAssignedPostingTo).HasColumnName("unAssignedPostingTo");

                entity.Property(e => e.UserUpdate).HasColumnName("userUpdate");

                entity.Property(e => e.VideoOnDemandPostsTo).HasColumnName("videoOnDemandPostsTo");

                entity.Property(e => e.VoidCancellation).HasColumnName("void_cancellation");

                entity.Property(e => e.VoidCancellationChargeTypeId).HasColumnName("voidCancellationChargeTypeID");

                entity.Property(e => e.VoidNoShow).HasColumnName("void_noShow");

                entity.Property(e => e.VoidNoShowChargeTypeId).HasColumnName("voidNoShowChargeTypeID");
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.ToTable("reservation", "dbo");

                entity.Property(e => e.ReservationId)
                    .HasColumnName("reservationID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId).HasColumnName("accountID");

                entity.Property(e => e.AdhocRateAmount)
                    .HasColumnName("adhocRateAmount")
                    .HasColumnType("numeric(19,4)");

                entity.Property(e => e.BillingAccountNumber)
                    .HasColumnName("billingAccountNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.BillingName)
                    .HasColumnName("billingName")
                    .HasMaxLength(50);

                entity.Property(e => e.BillingTypeId).HasColumnName("billingTypeID");

                entity.Property(e => e.BookedOn).HasColumnName("bookedOn");

                entity.Property(e => e.CancellationNumber).HasColumnName("cancellationNumber");

                entity.Property(e => e.CcLast4Digits)
                    .HasColumnName("ccLast4Digits")
                    .HasMaxLength(4);

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.ClientTypeId).HasColumnName("ClientTypeID");

                entity.Property(e => e.CompanyName)
                    .HasColumnName("companyName")
                    .HasMaxLength(50);

                entity.Property(e => e.ConfirmationNumber).HasColumnName("confirmationNumber");

                entity.Property(e => e.DateExpiration)
                    .HasColumnName("dateExpiration")
                    .HasMaxLength(50);

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.DoNotMove).HasColumnName("doNotMove");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate")
                    .HasColumnType("date");

                entity.Property(e => e.ExtConfirmationNumber).HasMaxLength(255);

                entity.Property(e => e.ExtcancellationNumber).HasMaxLength(50);

                entity.Property(e => e.FrequentGuestId).HasColumnName("frequentGuestID");

                entity.Property(e => e.GcAccountId).HasColumnName("gcAccountID");

                entity.Property(e => e.GcId).HasColumnName("gcID");

                entity.Property(e => e.GuestFirstName)
                    .HasColumnName("guestFirstName")
                    .HasMaxLength(50);

                entity.Property(e => e.GuestLastName)
                    .HasColumnName("guestLastName")
                    .HasMaxLength(50);

                entity.Property(e => e.HouseAccountId).HasColumnName("houseAccountID");

                entity.Property(e => e.ImportId)
                    .HasColumnName("importId")
                    .HasMaxLength(50);

                entity.Property(e => e.IsGroup).HasColumnName("isGroup");

                entity.Property(e => e.KeyCode).HasMaxLength(20);

                entity.Property(e => e.MarketSegmentValue).HasColumnName("marketSegmentValue");

                entity.Property(e => e.Notes2)
                    .HasColumnName("notes2")
                    .HasMaxLength(50);

                entity.Property(e => e.NumberOfAdults).HasColumnName("numberOfAdults");

                entity.Property(e => e.NumberOfChildren).HasColumnName("numberOfChildren");

                entity.Property(e => e.PaymentReferenceId).HasColumnName("paymentReferenceID");

                entity.Property(e => e.PhoneAreaCode)
                    .HasColumnName("phoneAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.PhoneExtension)
                    .HasColumnName("phoneExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phoneNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.PropertyTimeBookedOn).HasColumnName("PropertyTime_BookedOn");

                entity.Property(e => e.RatePlanId).HasColumnName("ratePlanID");

                entity.Property(e => e.RatepromoCode)
                    .HasColumnName("ratepromoCode")
                    .HasMaxLength(50);

                entity.Property(e => e.ReferralsValue).HasColumnName("referralsValue");

                entity.Property(e => e.Salutation)
                    .HasColumnName("salutation")
                    .HasMaxLength(25);

                entity.Property(e => e.SourceId).HasColumnName("sourceID");

                entity.Property(e => e.SpecialInstructions)
                    .HasColumnName("specialInstructions")
                    .HasMaxLength(255);

                entity.Property(e => e.StartDate)
                    .HasColumnName("startDate")
                    .HasColumnType("date");

                entity.Property(e => e.StatusId).HasColumnName("statusID");

                entity.Property(e => e.SubSourceId).HasColumnName("subSourceID");

                entity.Property(e => e.TaxExempt).HasColumnName("taxExempt");

                entity.Property(e => e.TaxExemptId)
                    .HasColumnName("taxExemptID")
                    .HasMaxLength(50);

                entity.Property(e => e.TransferResId).HasColumnName("transferResID");

                entity.Property(e => e.TravelAgentId).HasColumnName("travelAgentID");

                entity.Property(e => e.UseMailingInfo).HasColumnName("useMailingInfo");

                entity.Property(e => e.UserBooked).HasColumnName("userBooked");
            });

            modelBuilder.Entity<ReservationAddressRef>(entity =>
            {
                entity.HasKey(e => new { e.ReservationId, e.AddressTypeId })
                    .HasName("reservation_address_ref_pkey");

                entity.ToTable("reservation_address_ref", "dbo");

                entity.Property(e => e.ReservationId).HasColumnName("reservationID");

                entity.Property(e => e.AddressTypeId).HasColumnName("addressTypeID");

                entity.Property(e => e.Address1)
                    .HasColumnName("address1")
                    .HasMaxLength(50);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(50);

                entity.Property(e => e.Address3)
                    .HasColumnName("address3")
                    .HasMaxLength(50);

                entity.Property(e => e.AltPhoneAreaCode)
                    .HasColumnName("altPhoneAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.AltPhoneExtension)
                    .HasColumnName("altPhoneExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.AltPhoneNumber)
                    .HasColumnName("altPhoneNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.AltphoneCountryCode)
                    .HasColumnName("altphoneCountryCode")
                    .HasMaxLength(3);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(50);

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.ContactFirstName)
                    .HasColumnName("contactFirstName")
                    .HasMaxLength(50);

                entity.Property(e => e.ContactLastName)
                    .HasColumnName("contactLastName")
                    .HasMaxLength(50);

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.EMail)
                    .HasColumnName("eMail")
                    .HasMaxLength(100);

                entity.Property(e => e.FaxAreaCode)
                    .HasColumnName("faxAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.FaxCountryCode)
                    .HasColumnName("faxCountryCode")
                    .HasMaxLength(3);

                entity.Property(e => e.FaxExtension)
                    .HasColumnName("faxExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.FaxNumber)
                    .HasColumnName("faxNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.PhoneAreaCode)
                    .HasColumnName("phoneAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.PhoneCountryCode)
                    .HasColumnName("phoneCountryCode")
                    .HasMaxLength(3);

                entity.Property(e => e.PhoneExtension)
                    .HasColumnName("phoneExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phoneNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postalCode")
                    .HasMaxLength(10);

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.Salutation)
                    .HasColumnName("salutation")
                    .HasMaxLength(25);

                entity.Property(e => e.TerritoryId).HasColumnName("territoryID");
            });

            modelBuilder.Entity<ReservationLog>(entity =>
            {
                entity.HasKey(e => e.LogId)
                    .HasName("reservation_log_pkey");

                entity.ToTable("reservation_log", "dbo");

                entity.Property(e => e.LogId)
                    .HasColumnName("LogID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountId).HasColumnName("accountID");

                entity.Property(e => e.BillingAccountNumber)
                    .HasColumnName("billingAccountNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.BillingName)
                    .HasColumnName("billingName")
                    .HasMaxLength(50);

                entity.Property(e => e.BillingTypeId).HasColumnName("billingTypeID");

                entity.Property(e => e.BookedOn).HasColumnName("bookedOn");

                entity.Property(e => e.CancellationNumber).HasColumnName("cancellationNumber");

                entity.Property(e => e.CcLast4Digits)
                    .HasColumnName("ccLast4Digits")
                    .HasMaxLength(4);

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.CompanyName)
                    .HasColumnName("companyName")
                    .HasMaxLength(50);

                entity.Property(e => e.ConfirmationNumber).HasColumnName("confirmationNumber");

                entity.Property(e => e.DateExpiration)
                    .HasColumnName("dateExpiration")
                    .HasMaxLength(50);

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.DoNotMove).HasColumnName("doNotMove");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtConfirmationNumber).HasMaxLength(255);

                entity.Property(e => e.ExtcancellationNumber).HasMaxLength(50);

                entity.Property(e => e.FrequentGuestId).HasColumnName("frequentGuestID");

                entity.Property(e => e.GcAccountId).HasColumnName("gcAccountID");

                entity.Property(e => e.GcId).HasColumnName("gcID");

                entity.Property(e => e.GuestFirstName)
                    .HasColumnName("guestFirstName")
                    .HasMaxLength(50);

                entity.Property(e => e.GuestLastName)
                    .HasColumnName("guestLastName")
                    .HasMaxLength(50);

                entity.Property(e => e.HouseAccountId).HasColumnName("houseAccountID");

                entity.Property(e => e.IsGroup).HasColumnName("isGroup");

                entity.Property(e => e.MarketSegmentValue).HasColumnName("marketSegmentValue");

                entity.Property(e => e.Notes2)
                    .HasColumnName("notes2")
                    .HasMaxLength(50);

                entity.Property(e => e.NumberOfAdults).HasColumnName("numberOfAdults");

                entity.Property(e => e.NumberOfChildren).HasColumnName("numberOfChildren");

                entity.Property(e => e.PaymentReferenceId).HasColumnName("paymentReferenceID");

                entity.Property(e => e.PhoneAreaCode)
                    .HasColumnName("phoneAreaCode")
                    .HasMaxLength(3);

                entity.Property(e => e.PhoneExtension)
                    .HasColumnName("phoneExtension")
                    .HasMaxLength(4);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phoneNumber")
                    .HasMaxLength(18);

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.PropertyTimeBookedOn).HasColumnName("PropertyTime_BookedOn");

                entity.Property(e => e.RatePlanId).HasColumnName("ratePlanID");

                entity.Property(e => e.RatepromoCode)
                    .HasColumnName("ratepromoCode")
                    .HasMaxLength(50);

                entity.Property(e => e.ReferralsValue).HasColumnName("referralsValue");

                entity.Property(e => e.ReservationId).HasColumnName("reservationID");

                entity.Property(e => e.Salutation)
                    .HasColumnName("salutation")
                    .HasMaxLength(25);

                entity.Property(e => e.SecurityCode)
                    .HasColumnName("securityCode")
                    .HasMaxLength(7);

                entity.Property(e => e.SourceId).HasColumnName("sourceID");

                entity.Property(e => e.SpecialInstructions)
                    .HasColumnName("specialInstructions")
                    .HasMaxLength(255);

                entity.Property(e => e.StatusId).HasColumnName("statusID");

                entity.Property(e => e.SubSourceId).HasColumnName("subSourceID");

                entity.Property(e => e.TaxExempt).HasColumnName("taxExempt");

                entity.Property(e => e.TaxExemptId)
                    .HasColumnName("taxExemptID")
                    .HasMaxLength(50);

                entity.Property(e => e.TravelAgentId).HasColumnName("travelAgentID");

                entity.Property(e => e.UseMailingInfo).HasColumnName("useMailingInfo");

                entity.Property(e => e.UserBooked).HasColumnName("userBooked");

                entity.Property(e => e.UserUpdate)
                    .HasColumnName("userUpdate")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ReservationStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("reservation_status_pkey");

                entity.ToTable("reservation_status", "dbo");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasColumnName("statusName")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.ToTable("room", "dbo");

                entity.Property(e => e.RoomId)
                    .HasColumnName("roomID")
                    .ValueGeneratedNever();

                entity.Property(e => e.HousekeepingzoneId).HasColumnName("housekeepingzoneID");

                entity.Property(e => e.RmSortOrder).HasColumnName("rmSortOrder");

                entity.Property(e => e.RoomClassId).HasColumnName("roomClassID");

                entity.Property(e => e.RoomNumber)
                    .HasColumnName("roomNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.RoomsetId).HasColumnName("roomsetID");

                entity.Property(e => e.StationId)
                    .HasColumnName("StationID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<RoomClassPublished>(entity =>
            {
                entity.HasKey(e => e.RoomClassId)
                    .HasName("roomClass_published_pkey");

                entity.ToTable("roomClass_published", "dbo");

                entity.Property(e => e.RoomClassId)
                    .HasColumnName("roomClassID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LastUpdated).HasColumnName("last_updated");

                entity.Property(e => e.Policy).HasColumnName("policy");

                entity.Property(e => e.RoomClassDescription).HasColumnName("roomClassDescription");

                entity.Property(e => e.RoomClassName)
                    .IsRequired()
                    .HasColumnName("roomClassName")
                    .HasMaxLength(50);

                entity.Property(e => e.RoomClassPicture)
                    .HasColumnName("roomClassPicture")
                    .HasMaxLength(255);

                entity.Property(e => e.RoomclassAbrv)
                    .HasColumnName("roomclassAbrv")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<RoomClassRef>(entity =>
            {
                entity.HasKey(e => e.RoomClassId)
                    .HasName("roomClass_ref_pkey");

                entity.ToTable("roomClass_ref", "dbo");

                entity.Property(e => e.RoomClassId)
                    .HasColumnName("roomClassID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.MaxAdults).HasColumnName("maxAdults");

                entity.Property(e => e.MaxPersons).HasColumnName("maxPersons");

                entity.Property(e => e.PartiesAllowed).HasColumnName("partiesAllowed");

                entity.Property(e => e.PetsAllowed).HasColumnName("petsAllowed");

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.Published).HasColumnName("published");

                entity.Property(e => e.RmclSortOrder).HasColumnName("rmclSortOrder");

                entity.Property(e => e.RoomClassViewId).HasColumnName("roomClassViewID");

                entity.Property(e => e.RoomSizeUnit)
                    .HasColumnName("roomSizeUnit")
                    .HasMaxLength(15);

                entity.Property(e => e.RoomSizeValue).HasColumnName("roomSizeValue");

                entity.Property(e => e.SmokingAllowed).HasColumnName("smokingAllowed");

                entity.Property(e => e.UserUpdate).HasColumnName("userUpdate");
            });

            modelBuilder.Entity<RoomInventoryExclude>(entity =>
            {
                entity.HasKey(e => e.RowId)
                    .HasName("roomInventoryExclude_pkey");

                entity.ToTable("roomInventoryExclude", "dbo");

                entity.Property(e => e.RowId)
                    .HasColumnName("rowID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BlockId).HasColumnName("blockID");

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.DateEffective).HasColumnName("dateEffective");

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.PropertyId).HasColumnName("propertyID");

                entity.Property(e => e.ReservationId).HasColumnName("reservationID");

                entity.Property(e => e.RoomAssignmentNumber).HasColumnName("roomAssignmentNumber");

                entity.Property(e => e.RoomId).HasColumnName("roomID");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.UserUpdate)
                    .HasColumnName("userUpdate")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Source>(entity =>
            {
                entity.ToTable("source", "dbo");

                entity.Property(e => e.SourceId)
                    .HasColumnName("sourceID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.IsShow).HasColumnName("isShow");

                entity.Property(e => e.ParentId).HasColumnName("parentID");

                entity.Property(e => e.Policy).HasColumnName("policy");

                entity.Property(e => e.SourceName)
                    .IsRequired()
                    .HasColumnName("sourceName")
                    .HasMaxLength(255);

                entity.Property(e => e.SourceUrl)
                    .IsRequired()
                    .HasColumnName("sourceURL")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<SysLedgerAccountTypeLov>(entity =>
            {
                entity.HasKey(e => e.LedgerAccountTypeId)
                    .HasName("sys_ledgerAccountType_lov_pkey");

                entity.ToTable("sys_ledgerAccountType_lov", "dbo");

                entity.Property(e => e.LedgerAccountTypeId)
                    .HasColumnName("ledgerAccountTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.LedgerAccountTypeDescription)
                    .HasColumnName("ledgerAccountTypeDescription")
                    .HasMaxLength(100);

                entity.Property(e => e.LedgerAccountTypeName)
                    .IsRequired()
                    .HasColumnName("ledgerAccountTypeName")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Territory>(entity =>
            {
                entity.ToTable("territory", "dbo");

                entity.Property(e => e.TerritoryId)
                    .HasColumnName("territoryID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.RegionId).HasColumnName("regionID");

                entity.Property(e => e.TerritoryAbrv)
                    .IsRequired()
                    .HasColumnName("territoryABRV")
                    .HasMaxLength(50);

                entity.Property(e => e.TerritoryName)
                    .IsRequired()
                    .HasColumnName("territoryName")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UserRef>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("user_ref_pkey");

                entity.ToTable("user_ref", "dbo");

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.CurrentLoginTime).HasColumnName("currentLoginTime");

                entity.Property(e => e.DateUpdate).HasColumnName("dateUpdate");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.IsPropertyAlwaysAvailable).HasColumnName("isPropertyAlwaysAvailable");

                entity.Property(e => e.LastLoginTime).HasColumnName("lastLoginTime");

                entity.Property(e => e.LastLogoutTime).HasColumnName("lastLogoutTime");

                entity.Property(e => e.LoginId)
                    .HasColumnName("loginID")
                    .HasMaxLength(10);

                entity.Property(e => e.PasswordExpiryDate).HasColumnName("passwordExpiryDate");

                entity.Property(e => e.UserNameFirst)
                    .IsRequired()
                    .HasColumnName("userNameFirst")
                    .HasMaxLength(50);

                entity.Property(e => e.UserNameLast)
                    .IsRequired()
                    .HasColumnName("userNameLast")
                    .HasMaxLength(50);

                entity.Property(e => e.UserPassword)
                    .IsRequired()
                    .HasColumnName("userPassword")
                    .HasMaxLength(50);

                entity.Property(e => e.UserUpdate).HasColumnName("userUpdate");
            });

            modelBuilder.Entity<UserRefPropertyXref>(entity =>
            {
                entity.HasKey(e => new { e.UserRefId, e.PropertyId })
                    .HasName("user_ref_property_xref_pkey");

                entity.ToTable("user_ref_property_xref", "dbo");

                entity.Property(e => e.UserRefId).HasColumnName("user_ref_id");

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.IsDefault).HasColumnName("isDefault");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
