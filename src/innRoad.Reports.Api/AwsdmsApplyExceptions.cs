﻿using System;
using System.Collections.Generic;

namespace innRoad.Reports.Api
{
    public partial class AwsdmsApplyExceptions
    {
        public string TaskName { get; set; }
        public string TableOwner { get; set; }
        public string TableName { get; set; }
        public DateTime ErrorTime { get; set; }
        public string Statement { get; set; }
        public string Error { get; set; }
    }
}
