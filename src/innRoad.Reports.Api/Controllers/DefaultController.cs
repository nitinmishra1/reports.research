﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace innRoad.Reports.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        public int Get()
        {
            var context = new reportsContext();
            int @propertyId = 2986;
            int @clientId = 1836;
            //Property Information
            var propertyInfo = from prop in context.PropertyPublished
                               join proref in context.PropertyRef on prop.PropertyId equals proref.PropertyId
                               join prad in context.PropertyAddressPublished on prop.PropertyId equals prad.PropertyId
                               join terr in context.Territory on prad.TerritoryId equals terr.TerritoryId
                               into terrpapp
                               where prop.PropertyId == @propertyId && prad.AddressTypeId == 3
                                     && proref.ClientId == @clientId
                               select new
                               {
                                   prop.PropertyId,
                                   prop.PropertyName,
                                   prop.LegalName,
                                   prop.PropertyUrl,
                                   prop.PropertyLogo,
                                   prop.Policy,
                                   prad.ContactName
                               };

            foreach (var v in propertyInfo)
            {

            }
            return 0;
        }
    }
}